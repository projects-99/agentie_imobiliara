﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Agentie_imobiliara
{
    public partial class Form1 : Form
    {
        OleDbConnection conn;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string nume= textBox1.Text;
            string parola = textBox2.Text;
            if (nume != "" && parola != "")
            {
                string q = "SELECT * FROM persoane WHERE Username='" + nume + "'";
                OleDbCommand c = new OleDbCommand(q, conn);
                OleDbDataReader dr = c.ExecuteReader();
                if (dr.HasRows)
                {
                    q = "SELECT * FROM persoane WHERE Username='" + nume + "' AND Password='" + parola + "'";
                    c = new OleDbCommand(q, conn);
                    dr = c.ExecuteReader();
                    if (dr.HasRows)
                    {
                        dr.Read();
                        string tip = dr[2].ToString();
                        string nume_prenume = dr[3].ToString();
                        string id = dr[1].ToString();
                        string user= dr[4].ToString();
                        if (tip == "Administrator")
                        {
                            administrator f2 = new administrator();
                            f2.id = id;
                            f2.username = user;
                            f2.Text= nume_prenume;
                            f2.Show();
                            this.Visible = false; 
                        }
                        else if (tip == "Client")
                        {
                            client f3 = new client();
                            f3.username= user;
                            f3.Text= nume_prenume;
                            f3.Show();
                            this.Visible = false;
                        }
                       

                    }
                    else MessageBox.Show("Parola este gresita!");
                }
                else MessageBox.Show("Utilizator inexistent!");
            }
            else MessageBox.Show("Nu ați completat datele!");
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            string cs = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=tabel1.accdb";
            conn = new OleDbConnection(cs);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            inregistrare f5 = new inregistrare();
            this.Hide();
            f5.ShowDialog();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
